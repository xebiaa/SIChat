package chat;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.ip.tcp.TcpNetSendingMessageHandler;

public class Runner {
	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		StdInAdapter.refreshAdapter(context, new TcpNetSendingMessageHandler("localhost" , 8090));
	}
}
