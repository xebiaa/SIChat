package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.SingletonBeanRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.integration.channel.SubscribableChannel;
import org.springframework.integration.core.Message;
import org.springframework.integration.endpoint.EventDrivenConsumer;
import org.springframework.integration.ip.tcp.TcpNetSendingMessageHandler;
import org.springframework.integration.message.MessageBuilder;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

public class StdInAdapter implements ApplicationContextAware {

	private ConfigurableApplicationContext context;

	public Message<String> scanAndSend() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = br.readLine();
		try {
			if (line.startsWith("location :")) {
				context.stop();
				context.refresh();
				refreshAdapter(context, new TcpNetSendingMessageHandler(line.split(":")[1],
						Integer.parseInt(line.split(":")[2])));
				return null;
			} else {
				return MessageBuilder.withPayload(line).build();
			}
		} finally {
			if(!context.isRunning())
				context.start();
		}
	}

	public static void refreshAdapter(ConfigurableApplicationContext context,
			TcpNetSendingMessageHandler handler) {
		SubscribableChannel channel = (SubscribableChannel) context
				.getBean("senderChannel");
		SingletonBeanRegistry registry = context.getBeanFactory();
		EventDrivenConsumer consumer = new EventDrivenConsumer(channel,handler);
		consumer.setTaskScheduler(new ConcurrentTaskScheduler());
		registry.registerSingleton("outboundAdapter", consumer);
		context.start();
	}

	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		context = (ConfigurableApplicationContext) arg0;
	}

}
